from django.urls import path
from receipts.views import (
    show_receipt_item,
    create_receipt,
    category_list,
    account_list,
    create_account,
    create_category,
)


urlpatterns = [
    path("", show_receipt_item, name="show_receipt_item"),
    path("", show_receipt_item, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
